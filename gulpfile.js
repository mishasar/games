'use strict'

const path = require('path');
const gulp = require('gulp');
const $ = require('gulp-load-plugins')();
const fileinclude = require('gulp-file-include');
const combine = require('stream-combiner2');
const browserSync = require('browser-sync').create();
const rimraf = require('rimraf');

const ENV = {
	dev: $.environments.development,
	prod: $.environments.production
}

gulp.task('html', () => {
	let combined = combine.obj([
		gulp.src('./source/*.html'),
		gulp.dest('./www/')
	]);

	combined.on('error', console.error.bind(console));
	return combined;
});

gulp.task('styles', () => {
	let combined = combine.obj([
		gulp.src('./source/css/style.less'),
		ENV.dev($.sourcemaps.init()),
		$.less({
            relativeUrls: true
		}),
		$.autoprefixer({ cascade: false }),
		$.csscomb(),
		$.cssnano(),
		ENV.dev($.sourcemaps.write()),
		gulp.dest('./www/css/')
	]);

	combined.on('error', console.error.bind(console));
	return combined;
});

gulp.task('libs', () => {
	let combined = combine.obj([
		gulp.src('./source/js/libs.js'),
		fileinclude('@@'),
		$.uglify(),
		gulp.dest('./www/js/')
	]);

	combined.on('error', console.error.bind(console));
	return combined;
});

gulp.task('scripts', () => {
	let combined = combine.obj([
		gulp.src(['./source/js/*.js', '!./source/js/libs.js']),
		ENV.dev($.sourcemaps.init()),
		$.babel({
			presets: ['env'],
			plugins: ['transform-object-rest-spread']
		}),
		$.uglify(),
		ENV.dev($.sourcemaps.write()),
		gulp.dest('./www/js/')
	]);

	combined.on('error', console.error.bind(console));
	return combined;
});

gulp.task('img', () => {
	let combined = combine.obj([
		gulp.src('./source/img/**/*.*'),
		$.imagemin(),
		gulp.dest('./www/img/')
	]);

	combined.on('error', console.error.bind(console));
	return combined;
});

gulp.task('pictures', () => {
	let combined = combine.obj([
		gulp.src('./source/pictures/**/*.*'),
		$.imagemin(),
		gulp.dest('./www/pictures/')
	]);

	combined.on('error', console.error.bind(console));
	return combined;
});

gulp.task('icons', function () {
	return gulp
		.src('source/icons/**/*.svg')
		.pipe($.svgmin(function (file) {
			var prefix = path.basename(file.relative, path.extname(file.relative));

			return {
				plugins: [{
					cleanupIDs: {
					prefix: 'icon-' + prefix,
					minify: true
				}
			}]
		}
	}))
	.pipe($.cheerio({
		run: function ($, file) {
			$('style').remove();
		},
		parserOptions: { xmlMode: true }
	}))
	.pipe($.svgstore())
	.pipe(gulp.dest('./www/img'));
});

gulp.task('fonts', () => {
	let combined = combine.obj([
		gulp.src('./source/fonts/*.*'),
		gulp.dest('./www/fonts/')
	]);

	combined.on('error', console.error.bind(console));
	return combined;
});

// gulp.task('video', () => {
// 	let combined = combine.obj([
// 		gulp.src('./source/video/**/*.*'),
// 		gulp.dest('./local/templates/assets/video/')
// 	]);
//
// 	combined.on('error', console.error.bind(console));
// 	return combined;
// });

gulp.task('clean', (cb) => {
	rimraf('./www/', cb);
});

gulp.task('build', [
	'styles',
	'libs',
	'scripts',
	'img',
	'pictures',
	'icons',
	'fonts',
]);

gulp.task('watch', () => {

	$.watch(['source/css/**/*.*'], function() {
		gulp.start('styles');
		browserSync.reload();
	});

	$.watch(['source/js/vendor/*.*', 'source/js/libs.js'], function() {
		gulp.start('libs');
		browserSync.reload();
	});

	$.watch(['source/js/**/*.js', '!source/js/libs.js'], function() {
		gulp.start('scripts');
		browserSync.reload();
	});

	$.watch(['source/img/**/*.*'], function() {
		gulp.start('img');
		browserSync.reload();
	});

	$.watch(['source/pictures/**/*.*'], function() {
		gulp.start('pictures');
		browserSync.reload();
	});

	$.watch(['source/icons/**/*.*'], function() {
		gulp.start('icons');
		browserSync.reload();
	});

	$.watch(['source/fonts/**/*.*'], function() {
		gulp.start('fonts');
		browserSync.reload();
	});

	$.watch(['source/video/**/*.*'], function() {
		gulp.start('video');
		browserSync.reload();
	});
});

gulp.task('server', () => {
	browserSync.init({
		server: { baseDir: "./local/templates/assets/" },
		port: 9000
	});
});

gulp.task('default', ['build', 'watch']);